#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# ============================================================================
# $Id:$
# ============================================================================
## @file TestData6.py
#  Generate different type of backgrounds 
#  @author Vanya BELYAEV Ivan..Belyaev@itep.ru
#  @date   2014-12-10
#
#                    $Revision:$
#  Last modification $Date:$
#                 by $Author:$
# ============================================================================
"""
Helper dataset for Ostap tutorial:
  - J/psi-like along  the first axis,
  - D0-like gaussian along the second axis 
  - double exponential background  
"""
# ============================================================================
__version__ = "$Revision:$"
__author__  = "Vanya BELYAEV  Ivan.Belyaev@itep.ru"
__date__    = "2014-12-10"
# ============================================================================
import ROOT, random
from   Ostap.PyRoUts  import *
# ============================================================================
# logging
# ============================================================================
from AnalysisPython.Logger import getLogger
if __name__  in ( '__main__' , '__builtin__' ) :
    logger = getLogger( 'OstapTutor.TestData6')
else : logger = getLogger( __name__ )
logger.info ('Create 1D dataset(s) for Ostap Tutorial ')
# ============================================================================

x      =  ROOT.RooRealVar( 'x' , 'x' , 0 , 1 )
varset = ROOT.RooArgSet ( x )

import Ostap.FitModels as Models

signal = Models.Gauss_pdf ( 'GPDF' , mass = x , mean = 0.5 , sigma = 0.03 )

data1  = signal.pdf.generate ( varset , 10000  )
for i in range(10000) :
    x.setVal( random.uniform(0,1) )
    data1.add ( varset )
    
data2  = signal.pdf.generate ( varset , 10000  )
f2     = ROOT.TF1 ( funID() , 'x**2' , 0 , 1 ) 
for i in range(10000) :
    x.setVal( f2.GetRandom() )
    data2.add ( varset )
    
data3  = signal.pdf.generate ( varset , 10000  )
f3     = ROOT.TF1 ( funID() , '(x-1)**2' , 0 , 1 ) 
for i in range(10000) :
    x.setVal( f3.GetRandom() )
    data3.add ( varset )

data4  = signal.pdf.generate ( varset , 10000  )
f4     = ROOT.TF1 ( funID() , '1-x**2' , 0 , 1 ) 
for i in range(10000) :
    x.setVal( f4.GetRandom() )
    data4.add ( varset )

data5  = signal.pdf.generate ( varset , 10000  )
f5     = ROOT.TF1 ( funID() , '1-(x-1)**2' , 0 , 1 ) 
for i in range(10000) :
    x.setVal( f5.GetRandom() )
    data5.add ( varset )

data6  = signal.pdf.generate ( varset , 10000  )
f6     = ROOT.TF1 ( funID() , '1+tanh((x-0.35)*10)' , 0 , 1 ) 
for i in range(10000) :
    x.setVal( f6.GetRandom() )
    data6.add ( varset )


# ============================================================================
if '__main__' == __name__ :

    import Ostap.Line
    logger.info ( __file__ + '\n' + Ostap.Line.line )
    logger.info ( 80*'*'   )
    logger.info ( __doc__  )
    logger.info ( 80*'*'   )
    logger.info ( ' Author  : %s' %         __author__    )
    logger.info ( ' Version : %s' %         __version__   )
    logger.info ( ' Date    : %s' %         __date__      )
    logger.info ( ' Symbols : %s' %  list ( __all__     ) )
    logger.info ( 80*'*' )
    
# ============================================================================
# The END 
# ============================================================================
