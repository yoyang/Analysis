#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# ============================================================================
## @file ex11_data.py
#  Operations with trees/chains & datasets 
#  @author Vanya BELYAEV Ivan..Belyaev@itep.ru
#  @date   2014-12-10
#
# ============================================================================
""" Operations with Histograms 
"""
# ============================================================================
__version__ = "$Revision$"
__author__  = "Vanya BELYAEV  Ivan.Belyaev@itep.ru"
__date__    = "2014-12-10"
# ============================================================================
import ROOT
from   Ostap.PyRoUts import * 

import ex10_data as data

dataset = data.ds_Ypsi  ## get dataset 
chain   = data.chain    ## get chain 


## files in chain:
print '#FILES   : ', len(chain.files())

## branches in chain:
print 'BRANCHES : ', chain.branches

## entries in chain
print 'ENTRIES  : ', len(chain)

## get a smalelr chain (in terms of input files)
chain1 = chain[:1]

## another loop 
for i in range(100,200) :
    if chain1.GetEntry(i) <= 0 : break
    print i, chain1.mass 

## explicit loop over all entries in chain
for i in chain1 :
    print i.mass

## some statistic
print 'sumVar'  , chain.sumVar ('c2dtf<=5')
print 'sumVar'  , chain.sumVar ('c2dtf>5' )
print 'statVar' , chain.statVar('c2dtf'   )


#
## similar for dataset

for i in dataset :
    print i.mY 

print 'sumVar'  , dataset.sumVar ('mY<=10')
print 'sumVar'  , dataset.sumVar ('mY>10' )
print 'statVar' , dataset.statVar('mY'    )


print 'minmax: ', dataset.vminmax('mY')

dataset.draw('mY')
dataset.draw('mY','','hist same')


##projections

h1 = ROOT.TH1D(hID(),'',100,9,12)
h1.Draw()
dataset.project ( h1 , 'mY')

chain1.project ( h1 , 'mass1')



# ============================================================================
# The END 
# ============================================================================
