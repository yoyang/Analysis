#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# ============================================================================
## @file ex1_histos.py
#  Operations with histograms 
#  @author Vanya BELYAEV Ivan..Belyaev@itep.ru
#  @date   2014-12-10
#
# ============================================================================
""" Operations with Histograms 
"""
# ============================================================================
__version__ = "$Revision$"
__author__  = "Vanya BELYAEV  Ivan.Belyaev@itep.ru"
__date__    = "2014-12-10"
# ============================================================================
import ROOT
# ============================================================================
# logging
# ============================================================================
from AnalysisPython.Logger import getLogger
if __name__  in ( '__main__' , '__builtin__' ) : logger = getLogger( 'ex2_histos')
else : logger = getLogger( __name__ )
logger.info ('Operations with Histograms')
# ============================================================================
from Ostap.PyRoUts import * 

## create histo

h1  = ROOT.TH1D(hID(),'',20,0,10)

## content of bin 'i'
print 'h[10] : ' , h1[10]

## loop over histogram bins 
for i in h1 :
    h1[i] = i ## fill bin 'i' with value 'i' 

## another loop over the histogram content:
for i in h1.iteritems() :
    print i


## add 1+-1 to each historgam bin
h1 += VE(1,1) 

## multiply by function 
h1 *= lambda x : 1+0.25*((x-10)/10)**2

## add with constant
h2 = 1 + h1

## sum two histograms 
h3 = h1 + h2

## division 
h4 = h1 / h3

## division 
h4 = 1 / h4

## power
h5 = h4**4

def fun1(x) : return x*x
## sum with function 
h6 = fun1 + h4

ff = ROOT.TF1('f1','x+5',-10,10) 
## divide by the function 
h7 = h6 / ff 

## historam as function
print 'h1(3.1415) :' , h1(3.1415)

# print 'h1(3.1415) :' , h1(3.1415, interpolate=0)
# print 'h1(3.1415) :' , h1(3.1415, interpolate=1)
# print 'h1(3.1415) :' , h1(3.1415, interpolate=2)
# print 'h1(3.1415) :' , h1(3.1415, interpolate=3)


hh = ROOT.TH1D('hh','',60,0,30)
for i in hh : hh[i] = 5+(i/2)%3

## convert to TF1
f0 = hh.asTF1 ( interpolate=0 ) ## no interpolation 
f1 = hh.asTF1 ( interpolate=1 )
f2 = hh.asTF1 ( interpolate=2 )
f3 = hh.asTF1 ( interpolate=3 )

f0.SetLineColor(3)
f1.SetLineColor(2)
f2.SetLineColor(4)
f3.SetLineColor(8)

## some math
from LHCbMath.math_ve import *

## absolute value 
h1a = abs ( h1 )
## power 
h1s = h1**2.2 
## exp and any other functions 
h1s = exp(h1)

## running sum:
h = h1.sumv()
h = h1.sumv( increasing = True  ) ## default 
h = h1.sumv( increasing = False )

## "efficiency"
h = h1.effic()
h = h1.effic ( increasing=True  ) ## default
h = h1.effic ( increasing=False ) 


## accimulate, sum
print 'sum(2.1, 6.3) : ' , h1.sum(2.1,6.3), h1.accumulate()

## various stats
print 'mean             : ' , h1.mean     ()
print 'rms              : ' , h1.rms      ()
print 'skewness         : ' , h1.skewness ()
print 'kurtosis         : ' , h1.kurtosis ()
print 'moment(6)        : ' , h1.moment   (6)
print 'centralMoment(8) : ' , h1.centralMoment (8)
print 'stat             : ' , h1.stat     ()  ## see cpp.StatEntity 

## sample histogram
h = h1.sample()

## dump
print ' dump:' , h1.dump(60,20)

## smear the histogram (convolute with gaussian)
h = h1.smear ( 2 )

## solve equation "h1 == value "
print 'solve:  ' , h1.solve(10)


## parameterizations
pp = h1.positive  (5)  ## as a positive polynomial of 5th order (Bernstein)
p  = h1.polynomial(4)  ## as 4th order polynomial
l  = h1.legendre  (3)  ## as 3rd order Legendre polynomial
t  = h1.chebyshev (4)  ## as 4th order Tchebyshev polynomial
f  = h1.fourier   (8)  ## as 8th order Fourier sum 
c  = h1.cosine    (8)  ## as 8th order Fourier cosine  sum 




## efficiencies
fun = ROOT.TF1 ('ff','1-(x-1)**2',0,1)
import random 
hT  = ROOT.TH1F('hT','',20,0,1)  ## total or original'
hA  = hT.clone()                 ## acepted
hR  = hT.clone()                 ## rejected 
for i in range(10000) :
    v = fun.GetRandom()
    hT.Fill ( v )
    if random.uniform(0,1) < 0.3333 :  hA.Fill ( v )
    else                            :  hR.Fill ( v )  



## naive efficiency as accpeted/total
eff1 = hA / hT

## "Zech" efficiency 
eff3 = hA % hT 

## "safe" recipe:  e = hA / ( hA + hR ) = 1/(1+hR/hA) 
eff4 = 1/(1+hR/hA)

eff1.red  ()
eff3.blue ()
eff4.green() 


    

# ============================================================================
# The END 
# ============================================================================
