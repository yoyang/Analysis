#!/usr/bin/env ostap 
# -*- coding: utf-8 -*-
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# ============================================================================

## 1) import dataset and variable 
from OstapTutor.TestData4 import m_psi, m_D0, data

logger.info ( 'Data: %s' % data ) 

## 2) create the model: signal + background

import  Ostap.FitModels as Models

jpsi  = Models.Needham_pdf ( 'N' , mass = m_psi , mean = 3.096  , sigma = 0.013 )
D0    = Models.Gauss_pdf   ( 'G' , mass = m_D0  , mean = 1.864  , sigma = 0.007 )

model = Models.Fit2D (
    signal_1 = jpsi ,
    signal_2 = D0   ,
    power1   = 1    ,
    power2   = 1    ,
    powerA   = 1    ,
    powerB   = 1    )

## 3) fit it 
r,f = model.fitTo  ( data , ncpu = 8 , silent = True ) 

## 4) fit it again 
r,f = model.fitTo  ( data , ncpu = 8 , silent = True ) 

## draw projection for the first variable 
model.draw1 ( data  , nbins = 50 )

## draw projection for the second variable 
model.draw2 ( data  , nbins = 50 )




# ============================================================================
# The END 
# ============================================================================
