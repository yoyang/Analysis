#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# ============================================================================
## @file ex1_ve.py
#  Operations with ValueWithError
#  @see Gaudi::Math::ValueWithError
#  @author Vanya BELYAEV Ivan..Belyaev@itep.ru
#  @date   2014-12-10
#
# ============================================================================
""" Operations with ValueWithError
- see Gaudi::Math::ValueWithError
"""
# ============================================================================
__version__ = "$Revision$"
__author__  = "Vanya BELYAEV  Ivan.Belyaev@itep.ru"
__date__    = "2014-12-10"
# ============================================================================
import ROOT
# ============================================================================
# logging
# ============================================================================
from AnalysisPython.Logger import getLogger
if __name__  in ( '__main__' , '__builtin__' ) : logger = getLogger( 'ex1_ve')
else : logger = getLogger( __name__ )
logger.info ('Operations with "VE": Gaudi::Math::ValueWithError')
# ============================================================================
from   Ostap.PyRoUts  import VE, cpp 

## create object from value and the squared error (dispersion)
a = VE(10,10)
b = VE(20,20)
print 'a, b                 : ' , a , b

## simple operations 
print '+,-,*,/              : ' , a + b, a - b, a * b, a / b

## simple operations 
print 'addition             : ', a + 5, 5 + a
print 'subtraction          : ', a - 6, 6 + a
print 'multiplication       : ', a * 6, 6 * a
print 'division             : ', a / 6, 6 / a


## what about correlations ?
print 'uncorrelated         : ' , VE(10,10)+VE(10,10)
print 'uncorrelated         : ' , a+VE(a) 
print 'correlated           : ' , a+a 


## more about correlations: 
print 'addition             : ' , cpp.Gaudi.Math.sum      ( a , b , -1 ), cpp.Gaudi.Math.sum      ( a , b ), cpp.Gaudi.Math.sum      ( a , b , +1 )
print 'subtraction          : ' , cpp.Gaudi.Math.subtract ( a , b , -1 ), cpp.Gaudi.Math.subtract ( a , b ), cpp.Gaudi.Math.subtract ( a , b , +1 )
print 'multiplication       : ' , cpp.Gaudi.Math.multiply ( a , b , -1 ), cpp.Gaudi.Math.multiply ( a , b ), cpp.Gaudi.Math.multiply ( a , b , +1 )
print 'division             : ' , cpp.Gaudi.Math.divide   ( a , b , -1 ), cpp.Gaudi.Math.divide   ( a , b ), cpp.Gaudi.Math.divide   ( a , b , +1 )


## more complicated operations: pow 
print 'pow                  : ' , a**2, b**3 , 3**a , a**b 

## very frequent fragments:
print 'frac: a/(a+b)        : ' , a.frac(b)
print 'asym: (a-b)/(a+b)    : ' , a.asym(b)

## chi2 distance:
print 'chi2(a,b)            : ' , a.chi2(b)

## average:
print 'mean(a,b)            : ' , a.mean(b)


## various math
from LHCbMath.math_ve import *

print 'sqrt/cbrt            : ' , sqrt (a) , cbrt  (b)
print 'exp/exp2/expm1       : ' , exp  (a) , exp2  (b) , expm1 (a) 
print 'log/log10/log2/log1p : ' , log  (a) , log10 (b) , log2  (a) , log1p(a) 
print 'sin/cos/tan          : ' , sin  (a) , cos   (a) , tan   (a)
print 'sinh/cosh/tanh       : ' , sinh (a) , cosh  (a) , tanh  (a)
print 'asin/acos/atan       : ' , asin (a) , acos  (a) , atan  (a)
print 'asinh/acosh/atanh    : ' , asinh(a) , acosh (a) , atanh (a)
print 'erf/erfc/erfcx       : ' , erf  (a) , erfc  (a) , erfcx (a)
print 'gamma/tgamma/lgamma  : ' , gamma(a) , tgamma(a) , lgamma(a)


# ============================================================================
# The END 
# ============================================================================
