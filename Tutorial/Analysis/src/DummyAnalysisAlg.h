/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: DummyAnalysisAlg.h,v 1.1 2009-10-02 07:24:53 cattanem Exp $
#ifndef DUMMYANALYSISALG_H
#define DUMMYANALYSISALG_H 1

// Include files
// from DaVinci, this is a specialized GaudiAlgorithm
#include "Kernel/DaVinciTupleAlgorithm.h"

/** @class DummyAnalysisAlg DummyAnalysisAlg.h
 *
 *  Dummy algorithm to allow creation of empty library in Tutorial example
 *
 *  @author Marco Cattaneo
 *  @date   2009-10-02
 */
class DummyAnalysisAlg : public DaVinciTupleAlgorithm
{

public:

  /// Standard constructor
  DummyAnalysisAlg( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~DummyAnalysisAlg( ); ///< Destructor

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution
  StatusCode finalize  () override;    ///< Algorithm finalization

protected:

private:

};
#endif // DUMMYANALYSISALG_H
