#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
# @file test_pydf.py
# Test module for ostap/fitting/pypdf.py
# - It tests PyPDF construction
# ============================================================================= 
""" Test module for ostap/fitting/pypdf.py
- It tests ``pure-python'' PDF 
"""
# ============================================================================= 
__author__ = "Ostap developers"
__all__    = () ## nothing to import
# ============================================================================= 
import ROOT, random, math
import Ostap.RooFitDeco 
from   Ostap.Core      import VE, dsID
from   Ostap.FitBasic  import MASS
from   Ostap.PyPdf     import PyPDF
# =============================================================================
# logging 
# =============================================================================
from Ostap.Logger import getLogger
if '__main__' == __name__  or '__builtin__' == __name__ : 
    logger = getLogger ( 'test_pypdf' )
else : 
    logger = getLogger ( __name__ )
# =============================================================================
## make simple test mass 
mass     = ROOT.RooRealVar ( 'test_mass' , 'Some test mass' , 3.0 , 3.2 )

## book very simple data set
varset0   = ROOT.RooArgSet  ( mass )
dataset   = ROOT.RooDataSet ( dsID() , 'Test Data set-0' , varset0 )  

mmin,mmax = mass.minmax()

## fill it 
m = VE(3.100,0.015**2)
for i in xrange(0,10000) :
    mass.value = m.gauss () 
    dataset.add ( varset0 )

logger.info ('DATASET %s' % dataset )

NORM = 1.0 / math.sqrt ( 2 * math.pi )
# =============================================================================
## @class PyGauss
#  local ``pure-python'' PDF 
class PyGauss(MASS,PyPDF) :
    """Local ``pure-python'' PDF """    
    def __init__ ( self         ,
                   name         ,
                   xvar         ,
                   mean  = ( 3.080 , 3.05  , 5.15  ) ,
                   sigma = ( 0.010 , 0.005 , 0.025 ) ,
                   pdf   = None ) :
        
        MASS .__init__ ( self , name      , xvar , mean , sigma ) 
        PyPDF.__init__ ( self , self.name , ( self.xvar  ,
                                              self.mean  ,
                                              self.sigma ) , pdf = pdf )
        self.config = {
            'name'  : self.name ,
            'xvar'  : self.xvar ,
            'mean'  : self.mean ,
            'sigma' : self.mean ,
            'pdf'   : None        ## attention! 
            }
        
    ## the  main method 
    def evaluate ( self ) :
        varlist = self.varlist        
        x = float ( varlist [ 0 ] ) 
        m = float ( varlist [ 1 ] ) 
        s = float ( varlist [ 2 ] )        
        dx = ( x - m ) / s        
        return math.exp ( -0.5 * dx * dx ) * NORM / s 


# =============================================================================
isqrt2 = 1./math.sqrt(2.0)
def CDF ( x , mean = 0.0 , sigma = 1.0 ) :
    dx = ( x - mean ) / ( 1.0 * sigma ) 
    return 0.5 * ( 1.0 + math.erf ( dx * isqrt2 ) )

# =============================================================================
## @class PyGaussAI
#  local ``pure-python'' PDF with analytical integrals 
class PyGaussAI(PyGauss) :
    """Local ``pure-python'' PDF with analytical integrals """    
    def __init__ ( self         ,
                   name         ,
                   xvar         ,
                   mean  = (         3.05  , 5.15  ) ,
                   sigma = ( 0.010 , 0.005 , 0.025 ) ,
                   pdf   = None ) :

        PyGauss.__init__ (  self , name , xvar , mean , sigma , pdf = pdf )

    ## declare analytical integral 
    def get_analytical_integral ( self ) :
        """Declare the analytical integral"""
        
        x  = self.varlist[0]
        
        if self.matchArgs ( x ) : return 1 ## get the integration code
        
        return 0
    
    ## calculate the analytical integral 
    def analytical_integral ( self ) :
        """Calculate the analytical integral"""

        assert 1 == self.intCode , 'Invalid integration code!'
        
        vlist = self.varlist

        rn    = self.rangeName        
        xv    = vlist [ 0 ]        
        xmax  = xv.getMax ( rn )
        xmin  = xv.getMin ( rn )
        
        m     = float ( vlist [ 1 ] ) 
        s     = float ( vlist [ 2 ] )        
        
        return CDF ( xmax , m , s  ) - CDF ( xmin , m , s  )
            
# =============================================================================
## pygauss PDF
# =============================================================================
def test_pygauss() :
    
    logger.info ('Test PyGauss:  simple Gaussian signal' )
    
    gauss   = PyGauss( 'PyGauss'   , xvar = mass )
    gaussAI = PyGauss( 'PyGaussAI' , xvar = mass )

    r1 , f1 = gauss.fitTo ( dataset , draw = True , silent = True )
    r2 , f2 = gaussAI.fitTo ( dataset , draw = True , silent = True )

    logger.info ( 'RESULT      is %s' % r1 )
    logger.info ( 'RESULT (AI) is %s' % r2 )
    
    logger.info ( 'MEAN        is %s' % ( 'OK' if 3.098 < gauss  .mean  < 3.102 else 'not-OK') )
    logger.info ( 'SIGMA       is %s' % ( 'OK' if 0.013 < gauss  .sigma < 0.017 else 'not-OK') )   
    logger.info ( 'MEAN   (AI) is %s' % ( 'OK' if 3.098 < gaussAI.mean  < 3.102 else 'not-OK') )
    logger.info ( 'SIGMA  (AI) is %s' % ( 'OK' if 0.013 < gaussAI.sigma < 0.017 else 'not-OK') )
    
    
    
# =============================================================================
if '__main__' == __name__ :

    test_pygauss          () ## simple Gaussian PDF
    pass

# =============================================================================
# The END 
# ============================================================================= 
