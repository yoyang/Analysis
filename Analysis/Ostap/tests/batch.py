###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

logger.info  ( 'I am batch-script' ) 
logger.debug ( 'I am batch-script' ) 
print 'my Context:' , dir() 
h1  = ROOT.TH1D('h1','',10,0,10)
for i in h1 : h1[i] = VE(i,i)
h1 *= 10
h1.draw()
print ' here print canvas!!!'
canvas >> 'batch'
