/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id$
// ============================================================================
#ifndef ANALYSIS_PYSELECTORWITHCUTS_H
#define ANALYSIS_PYSELECTORWITHCUTS_H 1
// ============================================================================
// Include files
// ============================================================================
// Local
// ============================================================================
#include "Analysis/PySelector.h"
// ============================================================================
// forward decalrations
class TCut ; // ROOT
// ============================================================================
namespace Analysis
{
  // ==========================================================================
  class Formula ;
  // ==========================================================================
  /** @class PySelectorWithCuts Analysis/PySelectorWithCuts.h
   *
   *
   *  @author Vanya Belyaev
   *  @date   2013-05-06
   */
  class GAUDI_API SelectorWithCuts : public Selector
  {
    // ========================================================================
  public:
    // ========================================================================
    ClassDef ( Analysis::SelectorWithCuts , 1 );
    // ========================================================================
  public:
    // ========================================================================
    /// constructor
    SelectorWithCuts
    ( const std::string& cuts = "" ,
      TTree*             tree = 0  ,
      PyObject*          self = 0  ) ;
    SelectorWithCuts
    ( const TCut&        cuts      ,
      TTree*             tree = 0  ,
      PyObject*          self = 0  ) ;
    /// virtual destructor
    virtual ~SelectorWithCuts () ; // virtual destructor
    // ========================================================================
  public:
    // ========================================================================
    Bool_t Notify       () override;
    void   Init         ( TTree*   tree  ) override;
    void   Begin        ( TTree*   tree  ) override;
    void   SlaveBegin   ( TTree*   tree  ) override;
    Bool_t Process      ( Long64_t entry ) override;
    // ========================================================================
  public:
    // ========================================================================
    /// is formula OK ?
    bool ok () const  ; // is formula OK ?
    /// get the formula
    Analysis::Formula* formula () const { return fMyformula ; }
    /// get the formula
    const std::string& cuts    () const { return fMycuts    ; }
    /// event counter (useless for PROOF, useful for interactive python)
    unsigned long long event   () const { return m_event    ; }
    // ========================================================================
  private:
    // ========================================================================
    /// the selection formula
    std::string        fMycuts    ;
    Analysis::Formula* fMyformula ;
    /// event counter
    unsigned long long m_event    ; // event counter: useless for PROOF
    // ========================================================================
  };
  // ==========================================================================
} //                                                  end of namespace Analysis
// ============================================================================
//                                                                      The END
// ============================================================================
#endif // ANALYSIS_PYSELECTORWITHCUTS_H
// ============================================================================
