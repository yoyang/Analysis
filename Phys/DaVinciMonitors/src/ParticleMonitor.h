/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// Include files
// from DaVinci, this is a specialized GaudiAlgorithm
#include "Kernel/DaVinciAlgorithm.h"

/** @class ParticleMonitor ParticleMonitor.h
 *
 *  Plot PID quantities for final state daughters
 *
 *  @author Patrick Koppenburg
 *  @date   2008-12-04
 */
class ParticleMonitor : public DaVinciAlgorithm
{

public:

  /// Standard constructor
  ParticleMonitor( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution

private:

  /// Configure cut tools.
  StatusCode configureCut( LoKi::IHybridFactory* f, const std::string & s, LoKi::Types::Cut& c);

  /// Fill plots
  StatusCode fillPlots(const LHCb::Particle* d, const std::string & where);

private:

  /// Selection cut applied to all mothers
  LoKi::Types::Cut  m_mother   { LoKi::BasicFunctors<const LHCb::Particle*>::BooleanConstant(true) }; 

  /// Selection cut applied to peak mothers
  LoKi::Types::Cut  m_peak     { LoKi::BasicFunctors<const LHCb::Particle*>::BooleanConstant(true) }; 
  
  /// Selection cut applied to sideband mothers
  LoKi::Types::Cut  m_sideband { LoKi::BasicFunctors<const LHCb::Particle*>::BooleanConstant(false) };

  /// Selection cut applied to all mothers
  Gaudi::Property<std::string> m_motherCut 
  { this, "MotherCut",  "ALL",  "The cut to be applied to all mother particle (default all)." };

  /// Selection cut applied to peak mothers
  Gaudi::Property<std::string> m_peakCut         
  { this, "PeakCut", "ALL", "Selection of mass peak (default all)." };

  /// Selection cut applied to sideband mothers
  Gaudi::Property<std::string> m_sidebandCut    
  { this, "SideBandCut", "NONE", "Selection of sidebands (default none)." };

  /// names for plottols, only for initialize
  Gaudi::Property< std::vector<std::string> > m_plotToolNames
  { this, "PlotTools", { 1, "MomentumPlotTool" } };

  std::map<std::string,IPlotTool*> m_plotTools ; ///< Plot tools to be used
  std::string m_massPlotToolName{"MassPlotTool"} ; ///< Mass plot tool is special

};
