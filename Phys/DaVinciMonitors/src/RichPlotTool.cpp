/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// local
#include "RichPlotTool.h"

using namespace Gaudi::Units;
//-----------------------------------------------------------------------------
// Implementation file for class : RichPlotTool
//
// 2008-12-05 : Patrick Koppenburg
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
RichPlotTool::RichPlotTool( const std::string& type,
                            const std::string& name,
                            const IInterface* parent )
: BasePlotTool ( type, name , parent ) { }

//=============================================================================
// initialize
//=============================================================================
StatusCode RichPlotTool::initialize()
{
  const auto sc = BasePlotTool::initialize();
  if ( !sc ) return sc;

  // pre-load RichPlot tools for common trailer names ...
  // avoids excessive cpu during first few events
  pidTool("peak");
  pidTool("sideband");

  return sc;
}

//=============================================================================
// Daughter plots - just mass plots
//=============================================================================
StatusCode RichPlotTool::fillImpl( const LHCb::Particle* p,
                                   const std::string& trailer )
{
  // skip composite particles
  if ( p->isBasicParticle() )
  {

    // Get the info for this particle
    const auto prop = particleProperty( p->particleID() );
    if ( prop )
    {
      // fill the plots
      pidTool(trailer)->plots( p->proto(), pidType(prop) );
    }
    
  }
  
  // return
  return StatusCode::SUCCESS;
}

Rich::ParticleIDType
RichPlotTool::pidType( const LHCb::ParticleProperty * prop ) const
{
  Rich::ParticleIDType type = Rich::Unknown;
  if      ( abs(prop->pythiaID()) == 321        ) { type = Rich::Kaon; }
  else if ( abs(prop->pythiaID()) == 211        ) { type = Rich::Pion; }
  else if ( abs(prop->pythiaID()) == 2212       ) { type = Rich::Proton; }
  else if ( abs(prop->pythiaID()) == 11         ) { type = Rich::Electron; }
  else if ( abs(prop->pythiaID()) == 13         ) { type = Rich::Muon; }
  else if ( abs(prop->pythiaID()) == 1000010020 ) { type = Rich::Deuteron; }
  if ( UNLIKELY( type == Rich::Unknown ) )
  {
    std::ostringstream mess;
    mess << "Unknown RICH stable particle : " << prop->particle();
    Warning( mess.str() ).ignore();
  }
  return type;
}

IJobOptionsSvc* RichPlotTool::joSvc() const
{
  if ( UNLIKELY(!m_jos) )
  { m_jos = this -> svc<IJobOptionsSvc>( "JobOptionsSvc" ); }
  return m_jos;
}

const Rich::Future::Rec::IPIDPlots *
RichPlotTool::pidTool( const std::string & toolname ) const
{
  auto iT = m_pidTools.find(toolname);
  if ( iT == m_pidTools.end() )
  {
    const auto fullname = name()+"."+toolname;
    // refine the histogram path
    StringProperty sp1( "HistoTopDir", "" );
    //    StringProperty sp2( "HistoDir", '"'+name()+"/"+toolname+'"' );
    StringProperty sp2( "HistoDir", '"'+name()+"."+toolname+'"' );
    const auto sc = ( joSvc()->addPropertyToCatalogue( fullname, sp1 ).isSuccess() &&
                      joSvc()->addPropertyToCatalogue( fullname, sp2 ).isSuccess() );
    if ( !sc ) { Exception( "Error setting properties" ); }
    // get and return the tool
    return m_pidTools[toolname] = 
      tool<Rich::Future::Rec::IPIDPlots>("Rich::Future::Rec::PIDPlots",toolname,this);
  }
  return iT->second;
}

// Declaration of the Tool Factory
DECLARE_COMPONENT( RichPlotTool )
