/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "LoKi/PhysTypes.h"
#include "LoKi/IHybridFactory.h"
#include "Kernel/IPlotTool.h"

#include "ParticleMonitor.h"

//-----------------------------------------------------------------------------
// Implementation file for class : ParticleMonitor
//
// 2008-12-04 : Patrick Koppenburg
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( ParticleMonitor )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
ParticleMonitor::ParticleMonitor( const std::string& name,
                                  ISvcLocator* pSvcLocator)
  : DaVinciAlgorithm ( name , pSvcLocator ) { }

//=============================================================================
// Initialization
//=============================================================================
StatusCode ParticleMonitor::initialize()
{
  const auto sc = DaVinciAlgorithm::initialize();
  if ( !sc ) return sc;

  auto factory = tool<LoKi::IHybridFactory>
    ("LoKi::Hybrid::Tool/HybridFactory:PUBLIC" , this ) ;

  if ( !configureCut( factory, m_motherCut.value() , m_mother     ) ||
       !configureCut( factory, m_peakCut.value() , m_peak         ) ||
       !configureCut( factory, m_sidebandCut.value() , m_sideband ) )  
  { return StatusCode::FAILURE; }
  
  release(factory);

  if ( m_plotToolNames.empty() )
  {
    return Error( "No plot Tool defined. Will do nothing." ) ;
  }
  else
  {
    if (msgLevel(MSG::DEBUG)) debug() << "Using" ;
    m_plotTools.emplace( m_massPlotToolName, tool<IPlotTool>(m_massPlotToolName,this) );
    if (msgLevel(MSG::DEBUG)) debug() << " " << m_massPlotToolName << endmsg ;
    for ( const auto & s : m_plotToolNames )
    {
      if ( s == m_massPlotToolName )
      {
        // CRJ : No need to print this
        // warning() <<  *s << " is mandatory. Is already added to list." << endmsg ;
        continue ; // already done
      }
      m_plotTools.emplace( s, tool<IPlotTool>(s,this) );
      if (msgLevel(MSG::DEBUG)) debug() << " " << s << endmsg ;
    }
    if (msgLevel(MSG::DEBUG)) debug() << endmsg ;
  }

  return sc;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode ParticleMonitor::execute()
{
  for ( const auto m : particles() )
  {
    ++counter("# Mothers") ;
    if ( !m_mother( m ) )  { continue ; }   // discard particles with no cut

    std::string trail = "all";
    m_plotTools[m_massPlotToolName]->fillPlots(m,trail); // full mass plot

    ++counter("# Accepted Mothers");
    const bool peak     = m_peak( m ) ;
    const bool sideband = m_sideband( m ) ;

    if (peak)
    {
      ++counter("# Accepted Mothers in Peak");
      trail = "peak";
    }
    else if (sideband)
    {
      ++counter("# Accepted Mothers in Sidebands");
      trail = "sideband";
    }
    else continue ;  // ignore events ouside of sidebands

    if (!fillPlots(m,trail)) return StatusCode::FAILURE;
  }

  setFilterPassed(true);  // Mandatory. Set to true if event is accepted.
  return StatusCode::SUCCESS;
}

StatusCode ParticleMonitor::fillPlots( const LHCb::Particle* d,
                                       const std::string & where )
{
  for ( const auto & s : m_plotTools )
  {
    if (msgLevel(MSG::VERBOSE)) verbose() << "Filling " << s.first << endmsg ;
    if (!s.second->fillPlots(d,where)) return StatusCode::FAILURE;
  }
  return StatusCode::SUCCESS;
}

StatusCode ParticleMonitor::configureCut( LoKi::IHybridFactory* f,
                                          const std::string & s,
                                          LoKi::Types::Cut& c )
{
  const auto sc = f -> get ( s , c  ) ;
  if ( !sc ) { return Error ( "Unable to  decode cut: " + s  , sc ) ; }
  if ( msgLevel(MSG::DEBUG)) debug () << "The decoded cut is: " << s << endmsg ;
  return sc;
}

//=============================================================================
