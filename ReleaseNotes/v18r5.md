2017-08-28 Analysis v18r5
=========================

Release for 2017 production
---------------------------

This version is released on the 2017-patches branch.
It is based on Gaudi v28r2, LHCb v42r6, Lbcom v20r6, Rec v21r6 and Phys v23r6,
and uses LCG_88 with ROOT 6.08.06.

- Ostap: improvements, fixed and extra functionality.
  - See merge requests !159, !155, !154, !153, !151, !145, 144, !137, !133
- Ostap: fix all qmtests.
  - See merge request !162
- Code to add Herschel FOM in tuple tool.
  - See merge request !157
- Make Calo2MC tool public in BackgroundCategory and DaVinciSmartAssociator.
  - See merge request !156
- Fully qualify all enum value.
  - See merge request !146
- Add the muon correlated chi2 to TupleToolPid.
  - See merge request !152
- Add new options to TupleToolDecayTreeFitter.
  - See merge request !142
- DaVinciMCTools : Fix daughters logic in PrintDecayTreeTool.
  - See merge request !149
- DecayTreeTuple - Fix TupleToolGeometry inconsistent branches.
  - See merge request !147
- Small cleanup to TupleToolPi0Info.
  - See merge request !150
- Do not run by default the Yandex MVAs.
  - See merge request !138
- Add Tuple tool to investigate the associated clusters.
  - See merge request !134
