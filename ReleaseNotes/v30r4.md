2019-05-22 Analysis v30r4
=========================

Development release prepared on the master branch.
It is based on Gaudi v32r0, LHCb v50r4, Lbcom v30r4, Rec v30r4 and Phys v30r4, and uses LCG_95 with ROOT 6.16.00.

- Follow lhcb/LHCb!1770
  - See merge request lhcb/Analysis!497
